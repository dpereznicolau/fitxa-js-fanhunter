self.addEventListener('install', function(e) {
    e.waitUntil(
      caches.open('fitxa-fh').then(function(cache) {
        return cache.addAll([
          '/fitxa-js-fanhunter/',
          '/fitxa-js-fanhunter/index.html',
          '/fitxa-js-fanhunter/app.js',
          '/fitxa-js-fanhunter/style.css',
          '/fitxa-js-fanhunter/fh-logo.png',
          '/fitxa-js-fanhunter/picture.jpg'
        ]);
      })
    );
   });
   
   self.addEventListener('fetch', function(e) {
     console.log(e.request.url);
     e.respondWith(
       caches.match(e.request).then(function(response) {
         return response || fetch(e.request);
       })
     );
   });