//Regla especial segons arquetip escollit
var arquetipo = document.getElementById("Arquetipo");
var maxPointsHab = 0;
var minPointsHab = 0;
arquetipo.addEventListener("change", onArquetipoChanged);

function onArquetipoChanged(event) {
    unmarkAllHabilidades();
    var arquetipoValue = arquetipo.options[arquetipo.selectedIndex].value;
    switch (arquetipoValue) {
        case "Cerebrito":
            markHabilidades("cerebrito");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Fanzineroso":
            markHabilidades("fanzineroso");
            maxPointsHab = 12;
            minPointsHab = 6;
            break;
        case "Influencer":
            markHabilidades("influencer");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Investigador":
            markHabilidades("investigador");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Megafan":
            markHabilidades("megafan");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Mercenario":
            markHabilidades("mercenario");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Poli":
            markHabilidades("poli");
            maxPointsHab = 14;
            minPointsHab = 10;
            break;
        case "Super":
            markHabilidades("super");
            maxPointsHab = 7;
            minPointsHab = 7;
            break;
        default:
            maxPointsHab = 0;
            minPointsHab = 0;
    }

    var regla = arquetipo.options[arquetipo.selectedIndex].dataset.especial;
    document.getElementById("ReglaEspecial").value = regla;

    habilidadesPopover();
    unlockHabilities();
}

function unlockHabilities() {
    var habilidades = document.getElementsByClassName("habilidad");
    for (var i = 0; i < habilidades.length; i++) {
        habilidades[i].removeAttribute("readonly");
    }
}

function markHabilidades(arquetipo) {
    var habilidades = document.querySelectorAll("input." + arquetipo);
    for (var i = 0; i < habilidades.length; i++) {
        habilidades[i].classList.add("required");
    }
}

function unmarkAllHabilidades() {
    var habilidades = document.querySelectorAll("input.required");
    for (var i = 0; i < habilidades.length; i++) {
        habilidades[i].classList.remove("required");
    }
}

//Assignacio d'atributs i habilitats innatas
var hostias = document.getElementById("hostias");
var tiros = document.getElementById("tiros");
var musculos = document.getElementById("musculos");
var reflejos = document.getElementById("reflejos");
var neuronas = document.getElementById("neuronas");
var percepcion = document.getElementById("percepcion");
var agallas = document.getElementById("agallas");
var carisma = document.getElementById("carisma");
var puntos = 20 - parseInt(hostias.value) - parseInt(tiros.value) - parseInt(musculos.value) - parseInt(reflejos.value)
    - parseInt(neuronas.value) - parseInt(percepcion.value) - parseInt(agallas.value) - parseInt(carisma.value);

hostias.addEventListener("change", onHostiasChanged);
tiros.addEventListener("change", onTirosChanged);
musculos.addEventListener("change", onMusculosChanged);
reflejos.addEventListener("change", onReflejosChanged);
neuronas.addEventListener("change", onNeuronasChanged);
percepcion.addEventListener("change", onPercepcionChanged);
agallas.addEventListener("change", onAgallasChanged);
carisma.addEventListener("change", onCarismaChanged);

function checkAtributos() {
    var totalAtributos = parseInt(hostias.value) + parseInt(tiros.value) + parseInt(musculos.value) + parseInt(reflejos.value)
        + parseInt(neuronas.value) + parseInt(percepcion.value) + parseInt(agallas.value) + parseInt(carisma.value);

    return totalAtributos <= 20;
}

function calcPotra() {
    return parseInt(percepcion.value) + parseInt(agallas.value) + parseInt(carisma.value);
}

function onHostiasChanged(event) {
    var hostiasValue = parseInt(hostias.value);
    var defensa = document.getElementById("defensa");
    if (checkAtributos()) {
        var defensaVal = Math.floor(hostiasValue / 2);
        defensa.value = defensaVal;
        defensa.min = defensaVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        hostias.value = hostiasValue - 1;
        defensaVal = Math.floor((hostiasValue - 1) / 2)
        defensa.value = defensaVal;
        defensa.min = defensaVal;
    }
}

function onTirosChanged(event) {
    var tirosValue = parseInt(tiros.value);
    var lanzar = document.getElementById("lanzar");
    if (checkAtributos()) {
        var lanzarVal = Math.floor(tirosValue / 2);
        lanzar.value = lanzarVal;
        lanzar.min = lanzarVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        tiros.value = tirosValue - 1;
        lanzarVal = Math.floor((tirosValue - 1) / 2);
        lanzar.value = lanarVal;
        lanzar.min = lanzarVal;
    }
}

function onMusculosChanged(event) {
    var musculosValue = parseInt(musculos.value);
    var atletismo = document.getElementById("atletismo");
    if (checkAtributos()) {
        var atletismoVal = Math.floor(musculosValue / 2);
        atletismo.value = atletismoVal;
        atletismo.min = atletismoVal;
        document.getElementById("Pupas").value = musculosValue * 10;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        musculos.value = musculosValue - 1;
        atletismoVal = Math.floor((musculosValue - 1) / 2);
        atletismo.value = atletismoVal;
        atletismo.min = atletismoVal;
        document.getElementById("Pupas").value = (musculosValue - 1) * 10;
    }
}

function onReflejosChanged(event) {
    var reflejosValue = parseInt(reflejos.value);
    var cabriolas = document.getElementById("cabriolas");
    if (checkAtributos()) {
        var cabriolasVal = Math.floor(reflejosValue / 2);
        cabriolas.value = cabriolasVal;
        cabriolas.min = cabriolasVal;
        document.getElementById("Parada").value = reflejosValue * 4;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        reflejos.value = reflejosValue - 1;
        cabriolasVal = Math.floor((reflejosValue - 1) / 2);
        cabriolas.value = cabriolasVal;
        cabriolas.min = cabriolasVal;
        document.getElementById("Parada").value = (reflejosValue - 1) * 4;
    }
}

function onNeuronasChanged(event) {
    var neuronasValue = parseInt(neuronas.value);
    var investigar = document.getElementById("investigar");
    if (checkAtributos()) {
        var investigarVal = Math.floor(neuronasValue / 2);
        investigar.value = investigarVal;
        investigar.min = investigarVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        neuronas.value = neuronasValue - 1;
        investigarVal = Math.floor((neuronasValue - 1) / 2)
        investigar.value = investigarVal;
        investigar.min = investigarVal;
    }
}

function onPercepcionChanged(event) {
    var percepcionValue = parseInt(percepcion.value);
    var alerta = document.getElementById("alerta");
    if (checkAtributos()) {
        var alertaVal = Math.floor(percepcionValue / 2);
        alerta.value = alertaVal;
        alerta.min = alertaVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        percepcion.value = percepcionValue - 1;
        alertaVal = Math.floor((percepcionValue - 1) / 2);
        alerta.value = alertaVal;
        alerta.min = alertaVal;
    }
    document.getElementById("Potra").value = calcPotra();
}

function onAgallasChanged(event) {
    var agallasValue = parseInt(agallas.value);
    var sigilo = document.getElementById("sigilo");
    if (checkAtributos()) {
        var sigiloVal = Math.floor(agallasValue / 2);
        sigilo.value = sigiloVal;
        sigilo.min = sigiloVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        agallas.value = agallasValue - 1;
        sigiloVal = Math.floor((agallasValue - 1) / 2);
        sigilo.value = sigiloVal;
        sigilo.min = sigiloVal;
    }
    document.getElementById("Potra").value = calcPotra();
}

function onCarismaChanged(event) {
    var carismaValue = parseInt(carisma.value);
    var comecocos = document.getElementById("comecocos");
    if (checkAtributos()) {
        comecocosVal = Math.floor(carismaValue / 2);
        comecocos.value = comecocosVal;
        comecocos.min = comecocosVal;
        updatePuntos();
    } else {
        alert("La suma de todos los atributos iniciales no puede ser superior a 20");
        //retorn al valor anterior
        carisma.value = carismaValue - 1;
        comecocosVal = Math.floor((carismaValue - 1) / 2);
        comecocosVal = comecocosVal;
        comecocos.min = comecocosVal;
    }
    document.getElementById("Potra").value = calcPotra();
}

/* POPOVER TIPS */
var nombre = document.getElementById("Nombre");
nombre.addEventListener("focus", function (e) {
    e.target.parentElement.classList.remove("hide");
    e.target.parentElement.classList.add("show");
})
nombre.addEventListener("blur", function (e) {
    e.target.parentElement.classList.add("hide");
    setTimeout(function () {
        e.target.parentElement.classList.remove("show");
    }, 500);

})

var trasfondo = document.getElementById("Trasfondo");
trasfondo.addEventListener("focus", function (e) {
    e.target.parentElement.classList.remove("hide");
    e.target.parentElement.classList.add("show");
})
trasfondo.addEventListener("blur", function (e) {
    e.target.parentElement.classList.add("hide");
    setTimeout(function () {
        e.target.parentElement.classList.remove("show");
    }, 500);
})

document.getElementById("popover_atributos").innerText = "Reparte los " + puntos + " puntos restantes entre los 8 atributos. Recuerda que los atributos deben estar entre 1 y 5";

var atributosClass = document.getElementsByClassName("atributo");
for (var i = 0; i < atributosClass.length; i++) {
    atributosClass[i].addEventListener("focus", function (e) {
        e.target.closest(".popover__wrapper").classList.remove("hide");
        e.target.closest(".popover__wrapper").classList.add("show");
    })
    atributosClass[i].addEventListener("blur", function (e) {
        e.target.closest(".popover__wrapper").classList.add("hide");
    })
}

var arquetipo = document.getElementById("Arquetipo");
arquetipo.addEventListener("focus", function (e) {
    e.target.parentElement.classList.remove("hide");
    e.target.parentElement.classList.add("show");
})

arquetipo.addEventListener("blur", function (e) {
    e.target.parentElement.classList.add("hide");
    setTimeout(function () {
        e.target.parentElement.classList.remove("show");
    }, 500);
})

function updatePuntos() {
    puntos = 20 - parseInt(hostias.value) - parseInt(tiros.value) - parseInt(musculos.value) - parseInt(reflejos.value)
        - parseInt(neuronas.value) - parseInt(percepcion.value) - parseInt(agallas.value) - parseInt(carisma.value);
    if (puntos > 0) {
        document.getElementById("popover_atributos").innerText = "Reparte los " + puntos + " puntos restantes entre los 8 atributos. Recuerda que los atributos deben estar entre 1 y 5";
    } else {
        document.getElementById("popover_atributos").innerText = "Puntos de atributos repartidos, ahora selecciona el arquetipo del NJ";
        arquetipo.parentElement.classList.remove("hide");
        arquetipo.parentElement.classList.add("show");
    }
}

var habilidades = document.getElementsByClassName("habilidad");
var superpoderes = document.getElementsByClassName("superpoder");
for (var i = 0; i < habilidades.length; i++) {
    habilidades[i].addEventListener("change", habilidadesPopover);
}
for (var i = 0; i < superpoderes.length; i++) {
    superpoderes[i].addEventListener("change", habilidadesPopover);
}

function habilidadesPopover(e) {
    var sumaHabilidades = 0;
    var sumaRequired = 0;
    var sumaPoderes = 0;
    var habilidades = document.getElementsByClassName("habilidad");
    for (var i = 0; i < habilidades.length; i++) {
        var val = parseInt(habilidades[i].value);
        if (!isNaN(val)) {
            sumaHabilidades = sumaHabilidades + val - habilidades[i].min;
            if (habilidades[i].classList.contains("required")) {
                sumaRequired = sumaRequired + val - habilidades[i].min;
            }
        }
    }
    for (var i = 0; i < superpoderes.length; i++) {
        if (superpoderes[i].checked) {
            sumaPoderes += parseInt(superpoderes[i].dataset.cost);
        }
    }
    var actualHabilidades = maxPointsHab - sumaHabilidades;
    if (arquetipo.options[arquetipo.selectedIndex].value !== 'Super') {
        actualHabilidades -= sumaPoderes;
    }
    var actualRequired = minPointsHab - sumaRequired;
    if (actualHabilidades < 0 && e !== undefined) {
        var val = actualHabilidades
        e.target.value = parseInt(e.target.value) + val;
        actualHabilidades -= val;
        if (e.target.classList.contains("required")) {
            actualRequired -= val;
        }
    }
    var actualSuperpoderes = (actualHabilidades - (Math.max(actualRequired, 0)));
    if (arquetipo.options[arquetipo.selectedIndex].value === 'Super') {
        actualSuperpoderes = 20 - sumaPoderes;
    }
    var warningTxt = '';
    if ((Math.max(actualRequired, 0)) > actualHabilidades) {
        warningTxt = '<span id="warning">Cuidado! No podrás cumplir el requisito minimo de habilidades de arquetipo.<span>'
    }
    document.getElementById("popover_atributos").innerHTML =
        "Reparte los " + (actualHabilidades) + " puntos entre las habilidades que desees.<br> Recuerda que debes gastar un minimo de "
        + minPointsHab + " puntos en las habilidades de arquetipo (marcadas en azul).<br> Aun te faltan " + (Math.max(actualRequired, 0)) + " puntos por gastar."
        + "<br>" + warningTxt;

    document.getElementById("popover_superpoderes").innerHTML =
        "Tienes " + (actualSuperpoderes) + " puntos para adquirir Superpoderes";

    if (actualHabilidades === 0 && (Math.max(actualRequired, 0)) === 0) {
        document.getElementById("atributos").classList.remove("show");
        document.getElementById("atributos").classList.add("hide");
    } else {
        document.getElementById("atributos").classList.remove("hide");
        document.getElementById("atributos").classList.add("show");
    }
    if (actualSuperpoderes < 4) {
        document.getElementById("superpoderesWrapper").classList.remove("show");
        document.getElementById("superpoderesWrapper").classList.add("hide");
    } else {
        document.getElementById("superpoderesWrapper").classList.remove("hide");
        document.getElementById("superpoderesWrapper").classList.add("show");
    }
    // enable/disable superpoderes
    for (var i = 0; i < superpoderes.length; i++) {
        superpoderes[i].disabled = superpoderes[i].dataset.cost > actualSuperpoderes && !superpoderes[i].checked;
    }
}

var conyas = document.getElementsByClassName('conya');
var taras = document.getElementsByClassName('tara');
for (var i = 0; i < conyas.length; i++) {
    conyas[i].addEventListener('change', checkConyasTaras);
}
for (var i = 0; i < taras.length; i++) {
    taras[i].addEventListener('change', checkConyasTaras);
}

function checkConyasTaras(e) {
    var numConyas = 0;
    var numTaras = 0;
    for (var i = 0; i < conyas.length; i++) {
        if (conyas[i].checked) numConyas++;
    }
    for (var i = 0; i < taras.length; i++) {
        if (taras[i].checked) numTaras++;
    }
    if (numConyas === 3 || numTaras < numConyas - 1)
        disableConyas();
    else
        enableConyas();

    if (numTaras === 2)
        disableTaras();
    else
        enableTaras();
}

function disableConyas() {
    for (var i = 0; i < conyas.length; i++) {
        if (conyas[i].checked === false) conyas[i].disabled = true;
    }
}

function disableTaras() {
    for (var i = 0; i < taras.length; i++) {
        if (taras[i].checked === false) taras[i].disabled = true;
    }
}

function enableConyas() {
    for (var i = 0; i < conyas.length; i++) {
        conyas[i].disabled = false;
    }
}

function enableTaras() {
    for (var i = 0; i < taras.length; i++) {
        taras[i].disabled = false;
    }
}

var selectorsArma = document.getElementsByClassName('armaText');
for (var i = 0; i < selectorsArma.length; i++) {
    selectorsArma[i].addEventListener('change', fillWeaponAttr);
}

var weaponsMap = {
    blank: {
        alcance: '',
        cargador: '',
        manos: '',
        rafaga: '',
        danyo: '',
        reglasEsp: ''
    },
    ametralladora: {
        alcance: '50/100/200',
        cargador: '100',
        manos: '2',
        rafaga: 'Sí',
        danyo: '5d',
        reglasEsp: 'Ratatá, Trasto'
    },
    arco_ballesta: {
        alcance: '30/60/120',
        cargador: '-',
        manos: '2',
        rafaga: 'No',
        danyo: '3d',
        reglasEsp: '-'
    },
    escopeton: {
        alcance: '20/40/-',
        cargador: '8',
        manos: '2',
        rafaga: 'Sí',
        danyo: '3d',
        reglasEsp: 'Revientatripas'
    },
    fusilaco: {
        alcance: '40/80/160',
        cargador: '20',
        manos: '2',
        rafaga: 'Sí',
        danyo: '4d',
        reglasEsp: '-'
    },
    impaciente: {
        alcance: '30/60/120',
        cargador: '200',
        manos: '2',
        rafaga: 'Sí',
        danyo: '7d',
        reglasEsp: '¡Hijosdepuuutaaa!, Tormenta de balas'
    },
    lanzacohetes: {
        alcance: '20/40/80',
        cargador: '1',
        manos: '2',
        rafaga: 'No',
        danyo: '10d',
        reglasEsp: 'Antiblindaje, Explosión 6'
    },
    lanzagranadas: {
        alcance: '20/40/80',
        cargador: '4',
        manos: '2',
        rafaga: 'No',
        danyo: '8d',
        reglasEsp: 'Explosión 4'
    },
    lanzallamas: {
        alcance: '10/-/-',
        cargador: '6',
        manos: '2',
        rafaga: 'No',
        danyo: '5d',
        reglasEsp: 'Chorrazo 2, Incendiaria, Trasto'
    },
    pistolon: {
        alcance: '20/40/80',
        cargador: '10',
        manos: '1',
        rafaga: 'No',
        danyo: '3d',
        reglasEsp: 'Dos por una'
    },
    remendona: {
        alcance: '10/20/-',
        cargador: '2',
        manos: '2',
        rafaga: 'No',
        danyo: '5d',
        reglasEsp: 'Imprecisa, Zurriagazo'
    },
    francotirador: {
        alcance: '50/100/200',
        cargador: '10',
        manos: '2',
        rafaga: 'No',
        danyo: '5d',
        reglasEsp: 'Disparo lejano'
    },
    subfusil: {
        alcance: '20/40/80',
        cargador: '30',
        manos: '2',
        rafaga: 'Sí',
        danyo: '3d',
        reglasEsp: '-'
    },
    mano: {
        alcance: '-',
        cargador: '-',
        manos: '1',
        rafaga: '-',
        danyo: 'Músculos',
        reglasEsp: '-'
    },
    enorme: {
        alcance: '-',
        cargador: '-',
        manos: '2',
        rafaga: '-',
        danyo: 'Músc. + 2d',
        reglasEsp: 'Trasto'
    },
    larga: {
        alcance: '-',
        cargador: '-',
        manos: '1',
        rafaga: '-',
        danyo: 'Músc. + 1d',
        reglasEsp: '-'
    },
    desarmado: {
        alcance: '-',
        cargador: '-',
        manos: '1',
        rafaga: '-',
        danyo: 'Músc. - 1d',
        reglasEsp: '-'
    },
    katanote: {
        alcance: '-',
        cargador: '-',
        manos: '2',
        rafaga: '-',
        danyo: 'Músc. + 1d',
        reglasEsp: 'Barrido, Finura'
    },
    motosierra: {
        alcance: '-',
        cargador: '-',
        manos: '2',
        rafaga: '-',
        danyo: 'Músc. + 3d',
        reglasEsp: 'Ojocuidao, Trasto'
    },
    mano_arroja: {
        alcance: '5/10/20',
        cargador: '-',
        manos: '1',
        rafaga: 'Sí',
        danyo: 'Músculos',
        reglasEsp: '-'
    },
    improvisada: {
        alcance: 'Consulta manual',
        cargador: '-',
        manos: '1',
        rafaga: 'No',
        danyo: 'Músc. - 1d',
        reglasEsp: '-'
    },
    granada: {
        alcance: '5/10/20',
        cargador: '-',
        manos: '1',
        rafaga: 'No',
        danyo: '8d',
        reglasEsp: 'Explosión 5'
    },
    hacha: {
        alcance: '5/10/20',
        cargador: '-',
        manos: '1',
        rafaga: 'No',
        danyo: 'Músc. + 1d',
        reglasEsp: '-'
    },
    lanza: {
        alcance: '10/20/30',
        cargador: '-',
        manos: '1',
        rafaga: 'No',
        danyo: 'Músc. + 2d',
        reglasEsp: '-'
    },
    shuriken: {
        alcance: '5/10/20',
        cargador: '-',
        manos: '1',
        rafaga: 'Sí',
        danyo: '2d',
        reglasEsp: '-'
    }
}

function fillWeaponAttr() {
    var index = this.name.charAt(this.name.length - 1);
    document.querySelector('[name = alcance' + index + ']').value = weaponsMap[this.value]['alcance'];
    document.querySelector('[name = cargador' + index + ']').value = weaponsMap[this.value]['cargador'];
    document.querySelector('[name = manos' + index + ']').value = weaponsMap[this.value]['manos'];
    document.querySelector('[name = rafaga' + index + ']').value = weaponsMap[this.value]['rafaga'];
    document.querySelector('[name = danyo' + index + ']').value = weaponsMap[this.value]['danyo'];
    document.querySelector('[name = reglasEsp' + index + ']').value = weaponsMap[this.value]['reglasEsp'];
}

var butPrint = document.getElementById('butPrint');
butPrint.addEventListener('click', function (e) {
    e.preventDefault();
    window.print();
});

var form = document.querySelector('form');
form.addEventListener('reset', function () {
    location.reload();
});

var pict = document.getElementById('picture');
pict.addEventListener('change', function () {
    var reader = new FileReader();

    reader.onload = function () {
        document.getElementById('preview').src = reader.result;
    }

    reader.readAsDataURL(this.files[0]);
});

if('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('/fitxa-js-fanhunter/sw.js')
             .then(function() { console.log('Service Worker Registered'); });
}

let deferredPrompt;
const addBtn = document.querySelector('.add-button');
addBtn.style.display = 'none';

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI to notify the user they can add to home screen
  addBtn.style.display = 'inline-block';

  addBtn.addEventListener('click', (e) => {
    // hide our user interface that shows our A2HS button
    addBtn.style.display = 'none';
    // Show the prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt');
        } else {
          console.log('User dismissed the A2HS prompt');
        }
        deferredPrompt = null;
      });
  });
});